use super::opt::Writer;
use super::*;
use regex::Regex;
#[test]
fn test_grep() {
    struct Test<'a> {
        patterns: &'a [Regex],
        src: &'a str,
        want: &'a str,
        msg: &'a str,
    };

    fn regex(s: &str) -> Regex {
        Regex::new(s).unwrap()
    }

    let tests: &[Test] = &[
        Test {
            patterns: &[regex(r"[0-9]{3}-[0-9]{4}"), regex(r"[Ff]oo")],
            src: &["foo", "baz", "555-5555", "barFoo", "   "].join("\n"),
            want: &(["foo", "555-5555", "barFoo"].join("\n")),
            msg: "should support regex syntax",
        },
        Test {
            patterns: &[],
            src: "foof",
            want: "",
            msg: "given no patterns,* should match nothing",
        },
        Test {
            patterns: &[regex("")],
            src: "foo\nbar",
            want: "foo\nbar",
            msg: "empty regex should match everything",
        },
        Test {
            patterns: &[regex(r"^[a-z]+$"), regex(r"^[a-f]+$")],
            src: "abcd\nFOO\nab",
            want: "abcd\nab",
            msg: "overlapping patterns should give appropriate line only once",
        },
    ];

    for Test {
        patterns,
        src,
        want,
        msg,
    } in tests
    {
        let mut dst: Vec<u8> = Vec::with_capacity(want.len());
        grep(src.as_bytes(), &mut dst, &patterns).unwrap();
        let got = String::from_utf8(dst).unwrap();
        assert_eq!(got.trim(), want.trim(), "test: {}", msg);
    }
}
