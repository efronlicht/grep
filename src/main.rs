use std::fs::File;
use std::io;
use std::io::{BufRead, BufReader, Lines, Read, Write};
use walkdir::{DirEntry, WalkDir};
#[macro_use]
extern crate structopt;
use structopt::StructOpt;

use regex::Regex;
use std::path::{Path, PathBuf};
pub mod opt;
#[cfg(test)]
mod tests;
use opt::{InputMode, Opt};

fn main() -> io::Result<()> {
    let Opt {
        patterns,
        input_mode,
        remaining_files,
        out,
    } = Opt::from_args();
    _main(&patterns, &input_mode, &remaining_files, out)
}

pub fn _main(
    patterns: &[Regex],
    input: &InputMode,
    remaining_files: &[PathBuf],
    mut out: impl Write,
) -> io::Result<()> {
    match input {
        InputMode::Files(first) => {
            grep(File::open(first)?, &mut out, patterns)?;
            for path in remaining_files {
                grep(File::open(path)?, &mut out, patterns)?;
            }
            Ok(())
        }
        InputMode::Stdin => grep(io::stdin(), &mut out, patterns),
        InputMode::Recursive => {
            let walker = WalkDir::new("foo").into_iter();
            for entry in walker.filter_entry(|e| !is_hidden(e)) {
                grep(File::open(entry.unwrap().path())?, &mut out, patterns)?
            }
            Ok(())
        }
    }
}

fn is_hidden(entry: &DirEntry) -> bool {
    entry
        .file_name()
        .to_str()
        .map(|s| s.starts_with('.'))
        .unwrap_or(false)
}

pub fn grep(src: impl Read, mut dst: impl Write, patterns: &[Regex]) -> Result<(), io::Error> {
    for line_res in GrepIterator::new(src, &patterns) {
        let line = line_res?;
        writeln!(dst, "{}", line)?;
    }

    Ok(())
}

struct GrepIterator<'a, R> {
    patterns: &'a [Regex],
    lines: Lines<BufReader<R>>,
}

impl<'a, R: Read> Iterator for GrepIterator<'a, R> {
    type Item = Result<String, io::Error>;
    fn next(&mut self) -> Option<Self::Item> {
        match self.lines.next() {
            None => None,
            Some(Err(err)) => Some(Err(err)),
            Some(Ok(line)) => {
                if self.patterns.iter().any(|p| p.is_match(&line)) {
                    Some(Ok(line))
                } else {
                    self.next()
                }
            }
        }
    }
}

impl<'a, R: Read> GrepIterator<'a, R> {
    fn new(r: R, patterns: &'a [Regex]) -> Self {
        GrepIterator {
            patterns,
            lines: BufReader::new(r).lines(),
        }
    }
}
