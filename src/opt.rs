use regex::Regex;
use std::fs::File;
use std::io;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
pub struct Opt {
    #[structopt(parse(try_from_str = "parse_patterns"))]
    /// regexes to match, separated by newlines. this uses the finite automata regular expression engine specified by
    /// https://github.com/rust-lang/regex
    pub patterns: Box<[Regex]>,
    #[structopt(parse(from_str = "parse_input_mode"))]

    /// the input file to match. given no entry or "-", defaults to stdin
    pub input_mode: InputMode,

    #[structopt(parse(from_os_str))]
    pub remaining_files: Vec<PathBuf>,

    #[structopt(
        short = "o",
        long = "out",
        parse(try_from_str = "parse_out"),
        default_value = "-"
    )]
    pub out: io::BufWriter<Writer<File>>,
}

fn parse_input_mode(s: &str) -> InputMode {
    match s {
        "" => InputMode::Recursive,
        "-" => InputMode::Stdin,
        s => InputMode::Files(PathBuf::from(s)),
    }
}

#[derive(Debug)]
pub enum InputMode {
    Recursive,
    Files(PathBuf),
    Stdin,
}
pub fn parse_patterns(s: &str) -> Result<Box<[Regex]>, regex::Error> {
    s.lines().map(|s| s.parse::<Regex>()).collect()
}

pub fn parse_out(path: &str) -> io::Result<io::BufWriter<Writer<File>>> {
    let writer = if path == "__stdout__" {
        Writer::Stdout(io::stdout())
    } else {
        let file = File::create(path)?;
        Writer::Writer(file)
    };
    Ok(io::BufWriter::new(writer))
}

#[derive(Debug)]
pub enum Writer<W> {
    Writer(W),
    Stdout(io::Stdout),
}

impl<W: io::Write> io::Write for Writer<W> {
    #[inline]
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        match self {
            Writer::Writer(f) => f.write(buf),
            Writer::Stdout(s) => s.write(buf),
        }
    }
    #[inline]
    fn flush(&mut self) -> io::Result<()> {
        match self {
            Writer::Writer(f) => f.flush(),
            Writer::Stdout(s) => s.flush(),
        }
    }
}
